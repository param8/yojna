<?php 
class Ajax_controller extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
      $this->load->model('user_model');
      $this->load->model('course_model');
      $this->load->model('setting_model');
      $this->load->model('auth_model');
      $this->load->model('subject_model');
      $this->load->model('quiz_model');
      $this->load->model('test_series_model');
      $this->load->model('schedule_model');

	}

	public function get_city()
    {
      $userID = "";
      $userID = $this->input->post('userID');
      $stateID =  $this->input->post('stateID');
      if(!empty($userID)){
       $user = $this->user_model->get_user(array('users.id' => $userID));
      }
       
       $cities = $this->Common_model->get_city(array('state_id'=>$stateID));
       if(count($cities) > 0)
       {
        ?>
        <option value="">Select City</option>
        <?php foreach($cities as $city){ ?>
           <option value="<?=$city->id?>" <?=$user->city == $city->id ? 'selected' : '' ;?>><?=$city->name?></option>
        <?php
        }
       }else
       {
        ?>
        <option value="">No City found</option>
        <?php
       }
    }

    public function get_courses(){
      $topicID = "";
      $subjectID = "";
     
      if(!empty($this->input->post('subjectID'))){
        $subjectID = $this->input->post('subjectID');
        $subject = $this->subject_model->get_subject(array('subject.id'=>$subjectID));
      }

      if(!empty($this->input->post('topicID'))){
        $topicID = $this->input->post('topicID');
        $topic = $this->course_model->get_topic(array('topics.id'=>$topicID));
      }
      $categoryID = $this->input->post('categoryID');
      $courses = $this->course_model->get_courses(array('courses.categoryID'=>$categoryID,'courses.status'=>1));
      if(count($courses) > 0)
      {
       ?>
       <option value="">Select Course</option>
       <?php foreach($courses as $course){ 
        ?>
          <option value="<?=$course->id?>" <?=$topic->courseID == $course->id && !empty($topic) ? 'selected' : ($subject->courseID == $course->id && !empty($subject) ? 'selected' : '') ?>><?=$course->name?></option>
       <?php
       }
      }else
      {
       ?>
       <option value="">No Course found</option>
       <?php
      }
    }


    public function get_subject(){
      $topicID = "";
      $quizID = "";
      $testSeriesID = "";
      $scheduleID = "";
     
      if(!empty($this->input->post('topicID'))){
        $topicID = $this->input->post('topicID');
        $topic = $this->course_model->get_topic(array('topics.id'=>$topicID));
      }
      if(!empty($this->input->post('quizID'))){
        $quizID = $this->input->post('quizID');
        $quiz = $this->quiz_model->get_quiz(array('quiz.id'=>$quizID));
      }
      if(!empty($this->input->post('testSeriesID'))){
        $testSeriesID = $this->input->post('testSeriesID');
        $testSeries = $this->test_series_model->get_test_series(array('test_series.id'=>$testSeriesID));
      }
      if(!empty($this->input->post('scheduleID'))){
        $scheduleID = $this->input->post('scheduleID');
        $schedule = $this->schedule_model->get_schedule(array('id'=>$scheduleID));
      }

      $courseID =  $this->input->post('courseID');
      $subjects = $this->subject_model->get_subjects(array('subject.courseID'=>$courseID,'subject.status'=>1));
      if(count($subjects) > 0)
      {
       ?>
       <option value="">Select Subject</option>
       <?php foreach($subjects as $subject){ 
        ?>
          <option value="<?=$subject->id?>" <?=$topic->subjectID == $subject->id && !empty($topic) ? 'selected' : ($quiz->subjectID == $subject->id && !empty($quiz) ? 'selected' : ($testSeries->subjectID == $subject->id && !empty($testSeries) ? 'selected' : ($schedule->subjectID == $subject->id && !empty($schedule) ? 'selected' : ''))) ?>><?=$subject->name?></option>
       <?php
       }
      }else
      {
       ?>
       <option value="">No Course found</option>
       <?php
      }
    }

    public function get_email_otp(){
      $id = $this->session->userdata('id');
      $email = $this->input->post('email');
      $name = $this->session->userdata('name');
      $verification = $this->auth_model->get_verification(array('userID'=>$id));
      $email_otp = $verification->email_otp;
      if($verification){
        $subject = "Registartion Verification";
		    $html = 'Hello '.$name.',<br>Welcome to Shikshakul Classes.'.$email_otp.' is Your email verification code.<br><br>Best Regards,<br><br>Shikshakul Team';
			  $send =  sendEmail($email,$subject,$html);

        if($send==1){
          echo json_encode(['status'=>200, 'message'=>'OTP send email Successfully']);  
          exit();
        }else{
          echo json_encode(['status'=>403, 'message'=>'!OOPS OTP not send please try again']);  
          exit();
        }
      }

    }

    public function get_phone_otp(){
      $id = $this->session->userdata('id');
      $name = $this->session->userdata('name');
      $phone = $this->input->post('phone');
      $verification = $this->auth_model->get_verification(array('userID'=>$id));
      $mobile_otp = $verification->mobile_otp;
      if($verification){
        $templateID = '1707168795597484153';
        $message = urlencode('Hello '.$name.' Welcome to Shikshakul Classes '.$mobile_otp.' is your verification code.');
       $send = sendSMS($phone,$message,$templateID);
        if($send){
          echo json_encode(['status'=>200, 'message'=>'OTP send phone Successfully']);  
          exit();
        }else{
          echo json_encode(['status'=>403, 'message'=>'!OOPS OTP not send please try again']);  
          exit();
        }
      }
    }

    public function get_discount(){
      $apply_coupon = $this->input->post('apply_coupon');
      if(empty($apply_coupon)){
        echo json_encode(['status'=>402, 'message'=>'Please enter a coupon code']);  
        exit();
      }
      $cartItems=$this->cart->contents();
      $coupons = $this->setting_model->get_discount(array('course_discount.coupon'=>$apply_coupon));
      if($coupons){
        $courseID =array();
        $price =array();
        foreach($cartItems as $item){
          $courseID[] = $item['id'];
          $price[$item['id']] = $item['price'];
        }
      
        if(in_array($coupons->courseID,$courseID)){
          if($price[$coupons->courseID] >= $coupons->min_order){
             $sub_price =  $this->cart->total()-$price[$coupons->courseID];
             $price[$coupons->courseID];
             if($coupons->discount_type=='Percentage'){
               $discount = ($price[$coupons->courseID]*$coupons->discount)/100;
               $discounted_course_price = $price[$coupons->courseID]-$discount;
             }
             if($coupons->discount_type=='Amount'){
               $discounted_course_price = $price[$coupons->courseID]-$coupons->discount;
               $discount = $coupons->discount;
             }
             $addPrice = $sub_price+$discounted_course_price;
             $gst = ($addPrice*18)/100;
             $total_price = $addPrice+$gst;
             $discount_session = array(
              'course_id'   => $coupons->courseID,
              'addPrice'    => $addPrice,
              'gst'         => $gst,
              'total_price' => $total_price,
              'discount'    => $discount,
              'course_price'=> $discounted_course_price,
              'coupon_code'=> $apply_coupon,
             );
             $this->session->set_userdata($discount_session);
             echo json_encode(['status'=>200, 'price'=>$addPrice,'gst'=>$gst,'discount'=>$discount,'total_price'=>$total_price]); 

          }else{
            echo json_encode(['status'=>402, 'message'=>'Minimum order '.$coupons->min_order]);  
           exit();
          }
        
        }else{
          echo json_encode(['status'=>402, 'message'=>'This coupon code is not available for this course.']);  
           exit();
        }

      }else{
        echo json_encode(['status'=>403, 'message'=>'Invalid coupon']);  
        exit();
      }
    }

    public function reset_discount(){
      $this->session->unset_userdata('course_id');
      $this->session->unset_userdata('addPrice');
      $this->session->unset_userdata('gst');
      $this->session->unset_userdata('total_price');
      $this->session->unset_userdata('discount');
      $this->session->unset_userdata('course_price');
      $this->session->unset_userdata('coupon_code');
    }

    public function get_course_duration(){
      $start_date = $this->input->post('startDate');
      $end_date = $this->input->post('endDate');
      $diff= abs(strtotime($end_date) - strtotime($start_date));

      $days = floor(($diff)/ (60*60*24));
      echo json_encode(['status'=>200, 'duration'=>$days]); 
    }

    function set_order_type(){
     $value = $this->input->post('value');  
     $this->session->set_userdata('order_type',$value);
    }

  }
      

   
