<?php 
class Dashboard extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('auth_model');
	}

	public function index()
	{	 
		$data['page_title'] = 'Dashboard';
		
	  $this->admin_template('dashboard',$data);
		
	}



}