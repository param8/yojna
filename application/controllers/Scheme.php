<?php 
class Scheme extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('scheme_model');
	}

	public function index()
	{	 
		$data['page_title'] = 'Scheme';
		
	  $this->admin_template('scheme/scheme',$data);
		
	}


	public function ajaxScheme(){
		$condition = array('status'=>1);
		$schemes = $this->scheme_model->make_datatables_scheme($condition); // this will call modal function for fetching data
		$data = array();
		foreach($schemes as $key=>$scheme) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			$button .= '<a href="javascript:void(0)" onclick="editModal('.$scheme['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Eligibility Conditions" class="btn  btn-sm  text-warning"><i class="fa fa-edit"></i></a>';
			
			$sub_array[] = $key+1;
			$sub_array[] = $button;
			$sub_array[] = $scheme['name'];
			$sub_array[] = date('d-m-Y', strtotime($scheme['created_at']));
		
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->scheme_model->get_all_data_scheme($condition),
			"recordsFiltered"         =>     $this->scheme_model->get_filtered_data_scheme($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	}

	public function create_scheme(){
		$name = trim($this->input->post('name'));
		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter scheme']); 	
			exit();
		 }

		 $check = $this->scheme_model->get_scheme(array('name'=>$name));
		 
		 if($check){
			echo json_encode(['status'=>403, 'message'=>'This scheme already exist']); 	
			exit();
		 }
		
		 $data = array(
			'name' => $name,
		 );
    
		 $store = $this->scheme_model->store_scheme($data);
		 if($store){
			echo json_encode(['status'=>200, 'message'=>'Scheme added successfully!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
    }

	}

public function scheme_edit_form(){
	$id = $this->input->post('id');
	$scheme = $this->scheme_model->get_scheme(array('id'=>$id));
	?>
    <div class="row">
			<div class="col-xl-12">
			 <label class="col-lg-12 col-md-12 col-xl-12 col-form-label">Scheme</label>
				<div class="form-group row">
					<div class="col-lg-12 col-md-12 col-xl-12">
						<textarea type="text" class="form-control" id="name" name="name" ><?=$scheme->name?></textarea>
						<input type="hidden" id="id" name="id" value="<?=$id?>" >
					</div>
				</div>
			</div>
    </div>
	<?php 
}

public function update_scheme(){
	$id = $this->input->post('id');
	$name = trim($this->input->post('name'));
		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter scheme']); 	
			exit();
		 }

		 $check = $this->scheme_model->get_scheme(array('name'=>$name,'id<>'=>$id));
		 
		 if($check){
			echo json_encode(['status'=>403, 'message'=>'This scheme already exist']); 	
			exit();
		 }
		
		 $data = array(
			'name' => $name,
		 );
    
		 $update = $this->scheme_model->update_scheme($data,$id);
		 if($update){
			echo json_encode(['status'=>200, 'message'=>'Scheme updated successfully!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
    }

}


// Eligibility Conditions
	public function eligibility_conditions(){
		$data['page_title'] = 'Eligibility Conditions';
	  $this->admin_template('eligibility_conditions/eligibility-conditions',$data);
	}

	public function ajaxEligibilityConditions(){
		$condition = array('status'=>1);
		$conditions = $this->scheme_model->make_datatables_condition($condition); // this will call modal function for fetching data
		$data = array();
		foreach($conditions as $key=>$condition) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			$button .= '<a href="javascript:void(0)" onclick="editModal('.$condition['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Eligibility Conditions" class="btn  btn-sm  text-warning"><i class="fa fa-edit"></i></a>';
			
			$sub_array[] = $key+1;
			$sub_array[] = $button;
			$sub_array[] = $condition['name'];
			$sub_array[] = date('d-m-Y', strtotime($condition['created_at']));
		
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->scheme_model->get_all_data_condition($condition),
			"recordsFiltered"         =>     $this->scheme_model->get_filtered_data_condition($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	}

	public function create_eligibility_conditions(){
		$name = trim($this->input->post('name'));
		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter eligibility conditions']); 	
			exit();
		 }

		 $check = $this->scheme_model->get_eligibility_conditions(array('name'=>$name));
		 
		 if($check){
			echo json_encode(['status'=>403, 'message'=>'This eligibility conditions already exist']); 	
			exit();
		 }
		
		 $data = array(
			'name' => $name,
		 );
    
		 $store = $this->scheme_model->store_eligibility_conditions($data);
		 if($store){
			echo json_encode(['status'=>200, 'message'=>'Eligibility conditions added successfully!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
    }

	}

public function eligibility_conditions_edit_form(){
	$id = $this->input->post('id');
	$condition = $this->scheme_model->get_eligibility_conditions(array('id'=>$id));
	?>
    <div class="row">
			<div class="col-xl-12">
			 <label class="col-lg-12 col-md-12 col-xl-12 col-form-label">Eligibility Conditions</label>
				<div class="form-group row">
					<div class="col-lg-12 col-md-12 col-xl-12">
						<textarea type="text" class="form-control" id="name" name="name" ><?=$condition->name?></textarea>
						<input type="hidden" id="id" name="id" value="<?=$id?>" >
					</div>
				</div>
			</div>
    </div>
	<?php 
}

public function update_eligibility_conditions(){
	$id = $this->input->post('id');
	$name = trim($this->input->post('name'));
		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter eligibility conditions']); 	
			exit();
		 }

		 $check = $this->scheme_model->get_eligibility_conditions(array('name'=>$name,'id<>'=>$id));
		 
		 if($check){
			echo json_encode(['status'=>403, 'message'=>'This eligibility conditions already exist']); 	
			exit();
		 }
		
		 $data = array(
			'name' => $name,
		 );
    
		 $update = $this->scheme_model->update_eligibility_conditions($data,$id);
		 if($update){
			echo json_encode(['status'=>200, 'message'=>'Eligibility conditions updated successfully!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
    }

}

// Scheme Documentation

public function scheme_documents(){
	$data['page_title'] = 'Scheme Documents';
	$this->admin_template('scheme_documents/scheme-documents',$data);
}

public function ajaxSchemeDocuments(){
	$condition = array('status'=>1);
	$documents = $this->scheme_model->make_datatables_documents($condition); // this will call modal function for fetching data
	$data = array();
	foreach($documents as $key=>$document) // Loop over the data fetched and store them in array
	{
		$button = '';
		$sub_array = array();
		$button .= '<a href="javascript:void(0)" onclick="editModal('.$document['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Eligibility Conditions" class="btn  btn-sm  text-warning"><i class="fa fa-edit"></i></a>';
		
		$sub_array[] = $key+1;
		$sub_array[] = $button;
		$sub_array[] = $document['name'];
		$sub_array[] = date('d-m-Y', strtotime($document['created_at']));
	
		$data[] = $sub_array;
	}

	$output = array(
		"draw"                    =>     intval($_POST["draw"]),
		"recordsTotal"            =>     $this->scheme_model->get_all_data_documents($condition),
		"recordsFiltered"         =>     $this->scheme_model->get_filtered_data_documents($condition),
		"data"                    =>     $data
	);
	
	echo json_encode($output);
}

public function create_scheme_documents(){
	$name = trim($this->input->post('name'));
	if(empty($name)){
		echo json_encode(['status'=>403, 'message'=>'Please enter scheme documents']); 	
		exit();
	 }

	 $check = $this->scheme_model->get_scheme_documents(array('name'=>$name));
	 
	 if($check){
		echo json_encode(['status'=>403, 'message'=>'This scheme documents already exist']); 	
		exit();
	 }
	
	 $data = array(
		'name' => $name,
	 );
	
	 $store = $this->scheme_model->store_scheme_documents($data);
	 if($store){
		echo json_encode(['status'=>200, 'message'=>'Scheme documents added successfully!']);
	}else{
			echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
	}

}

public function scheme_documents_edit_form(){
$id = $this->input->post('id');
$document = $this->scheme_model->get_scheme_documents(array('id'=>$id));
?>
	<div class="row">
		<div class="col-xl-12">
		 <label class="col-lg-12 col-md-12 col-xl-12 col-form-label">Scheme Documents</label>
			<div class="form-group row">
				<div class="col-lg-12 col-md-12 col-xl-12">
					<textarea type="text" class="form-control" id="name" name="name" ><?=$document->name?></textarea>
					<input type="hidden" id="id" name="id" value="<?=$id?>" >
				</div>
			</div>
		</div>
	</div>
<?php 
}

public function update_scheme_documents(){
$id = $this->input->post('id');
$name = trim($this->input->post('name'));
	if(empty($name)){
		echo json_encode(['status'=>403, 'message'=>'Please enter scheme documents']); 	
		exit();
	 }

	 $check = $this->scheme_model->get_scheme_documents(array('name'=>$name,'id<>'=>$id));
	 
	 if($check){
		echo json_encode(['status'=>403, 'message'=>'This scheme documents already exist']); 	
		exit();
	 }
	
	 $data = array(
		'name' => $name,
	 );
	
	 $update = $this->scheme_model->update_scheme_documents($data,$id);
	 if($update){
		echo json_encode(['status'=>200, 'message'=>'Scheme documents updated successfully!']);
	}else{
			echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
	}

}

}