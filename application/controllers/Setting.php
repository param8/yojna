<?php
class Setting extends MY_Controller 
{
    public function __construct()
    {
      parent::__construct();
     $this->load->model('setting_model');
     $this->load->model('course_model');
     $this->load->library('csvimport');
    }

    public function index(){
      $data['page_title'] = 'General-Setting';
      $this->admin_template('components/breadcrumb',$data);
      $this->admin_template('setting/setting',$data);
    }

    public function payment_setting()
    {  	
        $this->not_admin_logged_in();
        $data['page_title'] = 'Payment-Setting';
        $data['payments'] = $this->Common_model->get_payment_info();
        $this->admin_template('components/breadcrumb',$data);
        $this->admin_template('setting/payment_setting',$data);
    }
    
    public function banner_setting()
    {  	
        $this->not_admin_logged_in();
        $data['page_title'] = 'Banner';
        $data['banners'] = $this->setting_model->get_banner();
        $this->admin_template('components/breadcrumb',$data);
        $this->admin_template('setting/banner_setting',$data);
    }

    public function about_setting()
    {  	
        $this->not_admin_logged_in();
        $data['page_title'] = 'About';
        $this->admin_template('components/breadcrumb',$data);
        $this->admin_template('setting/about_setting',$data);
    }

    public function terms_setting()
    {  	
        $this->not_admin_logged_in();
        $data['page_title'] = 'Terms-Condition';
        $data['siteinfo'] = $this->siteinfo();
        //$data['banners'] = $this->Common_model->get_banner();
        $this->admin_template('components/breadcrumb',$data);
        $this->admin_template('setting/terms_setting',$data);
    }

    public function privacy_setting()
    {  	
        $this->not_admin_logged_in();
        $data['page_title'] = 'Privacy-Policy';
        $data['siteinfo'] = $this->siteinfo();
        //$data['banners'] = $this->Common_model->get_banner();
        $this->admin_template('components/breadcrumb',$data);
        $this->admin_template('setting/privacy_setting',$data);
    }

    public function refund_setting()
    {  	
        $this->not_admin_logged_in();
        $data['page_title'] = 'Refund Policy';
        $this->admin_template('components/breadcrumb',$data);
        $this->admin_template('setting/refund_setting',$data);
    }

    public function email_setting()
    {  	
        $this->not_admin_logged_in();
        $data['page_title'] = 'Email-Setting';
        //$data['emailinfo'] = $this->siteinfo();
        $data['emailinfo'] = $this->Common_model->get_email_info();
        $this->admin_template('components/breadcrumb',$data);
        $this->admin_template('setting/email_setting',$data);
    }

    public function social_setting()
    {  	
        $this->not_admin_logged_in();
        $data['page_title'] = 'Social-Setting';
        //$data['siteinfo'] = $this->Common_model->get_site_info();
        $this->admin_template('components/breadcrumb',$data);
        $this->admin_template('setting/social_setting',$data);
    }

    public function seo_setting()
    {  	
        $this->not_admin_logged_in();
        $data['page_title'] = 'Seo-Setting';
        $data['seo'] = $this->Common_model->get_seo_info();
        $this->admin_template('components/breadcrumb',$data);
        $this->admin_template('setting/seo_setting',$data);
    }

    public function store_siteInfo(){
      //print_r($_FILES);print_r($_POST);die;
       $site_name = $this->input->post('site_name');
       $site_contact = $this->input->post('mobile');
       $whatsapp_no = $this->input->post('whatsapp');
       $site_email = $this->input->post('email');
       $site_address = $this->input->post('address');
       $footer_contant = $this->input->post('footer_content');
       $discription = $this->input->post('description');
       $siteinfo = $this->Common_model->get_site_info();
       if(empty($site_name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site name']); 	
        exit();
       }
       if(empty($site_contact)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site mobile']); 	
        exit();
       }
       if(empty($site_email)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site email']); 	
        exit();
       }

       if(strlen((string)$site_contact) != 10){
        echo json_encode(['status'=>403, 'message'=>'Please enter mobile number 10 digits']);
        exit();
       }
       if(!empty($whatsapp_no)){
       if(strlen((string)$whatsapp_no) != 10){
        echo json_encode(['status'=>403, 'message'=>'Please enter whatsapp number 10 digits']);
        exit();
       }
      }
      if(empty($footer_contant)){
        echo json_encode(['status'=>403, 'message'=>'Please enter footer content']); 	
        exit();
       }

       if(empty($discription)){
        echo json_encode(['status'=>403, 'message'=>'Please enter description']); 	
        exit();
       }

       
     $this->load->library('upload');
    if(!empty($_FILES['image']['name'])){
     $config = array(
      'upload_path' 	=> 'uploads/siteInfo',
      'file_name' 	=> str_replace(' ','',$site_name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif',
      'max_size' 		=> '10000000',
     );
     $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]); 	
          exit();
      }
      else
      {
        $type = explode('.', $_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/siteInfo/'.$config['file_name'].'.'.$type;
      }
    }elseif(!empty($siteinfo->site_logo)){
      $image = $siteinfo->site_logo;
    }else{
      $image = 'public/website/images/dummy_image.jpg';
    }
      $data = array(
       'site_name' => $site_name,
       'site_email' => $site_email,
       'site_contact' => $site_contact,
       'whatsapp_no' => $whatsapp_no,
       'site_address' => $site_address,
       'discription' => $discription,
       'footer_contant' => $footer_contant,
       'site_logo' => $image,
       'facebook_url' => $facebook_url,
       'youtube_url' => $youtube_url,
       'linkedin_url' => $linkedin_url,
       'twitter_url' => $twitter_url,
       'insta_url' => $insta_url,
      
      );

      $update = $this->setting_model->update_siteInfo($data);

      if($update){
        echo json_encode(['status'=>200, 'message'=>'Site-info update successfully!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
    }

    }

    public function store_banner(){
      //print_r($_POST);print_r($_FILES);
      $title = $this->input->post('title');
      $url = $this->input->post('url');
      $img_text = $this->input->post('img_text');


      $this->load->library('upload');
       $config = array(
        'upload_path' 	=> 'uploads/banner',
        'file_name' 	=> str_replace(' ','',$title).uniqid(),
        'allowed_types' => 'jpg|jpeg|png|gif',
        'max_size' 		=> '10000000',
       );
       $this->upload->initialize($config);
      if ( ! $this->upload->do_upload('image'))
        {
            $error = $this->upload->display_errors();
            echo json_encode(['status'=>403, 'message'=>$error]); 	
            exit();
        }
        else
        {
          $type = explode('.', $_FILES['image']['name']);
          $type = $type[count($type) - 1];
          $image = 'uploads/banner/'.$config['file_name'].'.'.$type;
        }

        $data = array(
          'banner_title' => $title,
          'link_url' => $url,
          'banner_img' => $image,
          'img_text' => $img_text,     
         );
   
         $update = $this->setting_model->insert_banner($data);
   
         if($update){
           echo json_encode(['status'=>200, 'message'=>'Banner Inserted successfully!']);
       }else{
           echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
       }

    }

    public function updateStatus(){
      $id = $this->input->post('id');
      $status = $this->input->post('status');
      if($status == 1){
        $data = array('status' => 0);
      }else{
        $data= array('status' => 1);
      }
    $update = $this->setting_model->update_status($data,$id);
    if($update){
      echo json_encode(['status'=>200, 'message'=>'Banner updated successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
    }
    
    public function deleteBanner(){
      $id = $this->input->post('id');
      $delete = $this->setting_model->delete_banner($id);
    
      if($delete){
        echo json_encode(['status'=>200, 'message'=>'Banner deleted successfully!']);
       }else{
           echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
       }
    }
    public function store_about(){

      $about_us = $this->input->post('about');
       if(empty($about_us)){
        echo json_encode(['status'=>403, 'message'=>'Please enter About Detail']); 	
        exit();
       }

      $data = array(
        'about_info' => $about_us,
       );
    // print_r($data);die;
       $update = $this->setting_model->update_siteInfo($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'About data updated successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
 

    }

    public function store_terms(){
      $terms = $this->input->post('terms');
       if(empty($terms)){
        echo json_encode(['status'=>403, 'message'=>'Please Enter Terms Conditions']); 	
        exit();
       }

      $data = array(
        'terms_of_use' => $terms,
       );
    // print_r($data);die;
       $update = $this->setting_model->update_siteInfo($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'Terms Conditions updated successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
 
    }

    public function store_privacy(){
      //print_r($_POST);die;
      $privacy = $this->input->post('privacy');
       if(empty($privacy)){
        echo json_encode(['status'=>403, 'message'=>'Please Enter Privacy Policy']); 	
        exit();
       }

      $data = array(
        'privacy_policy' => $privacy,
       );
    // print_r($data);die;
       $update = $this->setting_model->update_siteInfo($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'Privacy Policy updated successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
    }

    public function store_refund(){
      $refund_policy = $this->input->post('refund');
       if(empty($refund_policy)){
        echo json_encode(['status'=>403, 'message'=>'Please Enter Refund Policy']); 	
        exit();
       }

      $data = array(
        'refund_policy' => $refund_policy,
       );
    // print_r($data);die;
       $update = $this->setting_model->update_siteInfo($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'Refund Policy updated successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
    }

    public function store_payment(){
      $razor_key = $this->input->post('razor_key');
      $status = ($this->input->post('status') !='')?1:0;
      $paymnt_info = $this->Common_model->get_payment_info();
      $this->load->library('upload');
      if(!empty($_FILES['payment_logo']['name'])){
       $config = array(
        'upload_path' 	=> 'uploads/payment',
        'file_name' 	=> str_replace(' ','','razor_logo').uniqid(),
        'allowed_types' => 'jpg|jpeg|png|gif',
        'max_size' 		=> '10000000',
       );
       $this->upload->initialize($config);
      if ( ! $this->upload->do_upload('payment_logo'))
        {
            $error = $this->upload->display_errors();
            echo json_encode(['status'=>403, 'message'=>$error]); 	
            exit();
        }
        else
        {
          $type = explode('.', $_FILES['payment_logo']['name']);
          $type = $type[count($type) - 1];
          $image = 'uploads/payment/'.$config['file_name'].'.'.$type;
        }
      }elseif(!empty($paymnt_info->payment_logo)){
        $image = $paymnt_info->payment_logo;
      }else{
        $image = 'public/website/images/dummy_image.jpg';
      }
      $data = array(
        'razopay_key' => $razor_key,
        'payment_logo' => $image,
        'status' => $status,
       );
    // print_r($data);die;
       $update = $this->setting_model->update_paymentInfo($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'Payment-Setting update successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
 
    }

    public function store_socialmedia(){
      //print_r($_POST);
      $facebook_url = $this->input->post('facebook');
      $insta_url = $this->input->post('instagram');
      $twitter_url = $this->input->post('twitter');
      $linkedin_url = $this->input->post('linkedin');
      $youtube_url = $this->input->post('youtube');
      //  if(empty($site_name)){
      //   echo json_encode(['status'=>403, 'message'=>'Please enter site name']); 	
      //   exit();
      //  }
      $siteinfo = $this->Common_model->get_site_info();

      $data = array(
        'facebook_url' => $facebook_url,
        'insta_url' => $insta_url,
        'twitter_url' => $twitter_url,
        'linkedin_url' => $linkedin_url,
        'youtube_url' => $youtube_url, 
       );
    // print_r($data);die;
       $update = $this->setting_model->update_siteInfo($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'Site-info update successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
 

    }

    public function store_seo_form(){
      $title = $this->input->post('meta_title');
      $alt_text = $this->input->post('img_alt_text');
      $keyword = $this->input->post('keywords');
      $description = $this->input->post('description');
      if(empty($title)){
        echo json_encode(['status'=>402, 'message'=>'Please Enter Meta Title']);  
      }
      if(empty($alt_text)){
        echo json_encode(['status'=>402, 'message'=>'Please Enter Image Alt Text']);  
      }
      if(empty($keyword)){
        echo json_encode(['status'=>402, 'message'=>'Please Enter Keywords']);  
      }
      if(empty($description)){
        echo json_encode(['status'=>402, 'message'=>'Please Enter Description']);  
      }
      $data = array(
        'meta_title' => $title,
        'img_alt_text' => $alt_text,
        'meta_keywords' => $keyword,
        'meta_description' => $description,
       );
    // print_r($data);die;
       $update = $this->setting_model->update_seoInfo($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'Site-info update successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
    }

  public function store_email_form(){
   // print_r($_POST);die;

    $email_address = $this->input->post('from_address');
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $from_name = $this->input->post('from_name');    
    $host = $this->input->post('host');
    $port = $this->input->post('port');
    $status = ($this->input->post('status') !='')?1:0;
    //$status = $this->input->post['status'];

    $emailinfo = $this->Common_model->get_email_info();

      $data = array(
        'email_address' => $email_address,
        'email_host' => $host,
        'email_port' => $port,
        'email_name' => $from_name,
        'user_name' => $username, 
        'email_pswrd' => $password,
        'status' => $status,
       );
     //print_r($data);die;
       $update = $this->setting_model->update_email_info($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'Email-setting update successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
 

  }


  public function current_affairs() {
    $this->not_admin_logged_in();
    $data['page_title'] = 'Current Affairs';
    $this->admin_template('setting/current-affairs',$data);
  }

  public function ajaxCurrentAffires(){
    $this->not_admin_logged_in();
		$condition = array('status'=>1);
		$affairs = $this->setting_model->make_datatables_affairs($condition); // this will call modal function for fetching data
		$data = array();
		foreach($affairs as $key=>$affair) // Loop over the data fetched and store them in array
		{
    $button = '';
			$sub_array = array();
      $button .= '<a href="'.base_url('edit-current-affairs/'.base64_encode($affair['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Update current affiares" class="btn btn-sm bg-primary-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_current_affairs('.$affair['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete current affiares" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			
			$sub_array[] = $key+1;
      $sub_array[] = $button;
      $sub_array[] = '<img src="'.base_url($affair['image']).'" style="height:100px; width:100px;">';
			$sub_array[] = $affair['title'];
      $sub_array[] = $affair['type'];
			// $sub_array[] = $affair['description'];
			$sub_array[] = date('d-F-Y', strtotime($affair['created_at']));
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->setting_model->get_all_data_affairs($condition),
			"recordsFiltered"         =>     $this->setting_model->get_filtered_data_affairs($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
  }

  public function create_current_affairs(){
    $data['page_title'] = 'Create Current Affairs';
    $this->admin_template('setting/create-current-affairs',$data);
  }

  public function edit_current_affairs(){
    $data['page_title'] = 'Edit Current Affairs';
    $id = base64_decode($this->uri->segment(2));
    $data['affair'] = $this->setting_model->get_current_affairs(array('id' => $id));
    $this->admin_template('setting/edit-current-affairs',$data);
  }

  public function store_current_affairs(){
    $title = $this->input->post('title');
    $currentType = $this->input->post('currentType');
    $description = $this->input->post('description');
    $date = $this->input->post('date');
    $slug =  str_replace(' ','-',strtolower($title));
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }
     if(empty($currentType)){
      echo json_encode(['status'=>402, 'message'=>'Please select a type']);
      exit();
     }
     if(empty($description)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a description']);
      exit();
     }
     if(empty($date)){
      echo json_encode(['status'=>402, 'message'=>'Please select a date']);
      exit();
     }

     $this->load->library('upload');
     if(!empty($_FILES['image']['name'])){
      $config = array(
       'upload_path' 	=> 'uploads/current_affair',
       'file_name' 	=> str_replace(' ','','current_affairs').uniqid(),
       'allowed_types' => 'jpg|jpeg|png|gif',
       'max_size' 		=> '10000000',
      );
      $this->upload->initialize($config);
     if ( ! $this->upload->do_upload('image'))
       {
           $error = $this->upload->display_errors();
           echo json_encode(['status'=>403, 'message'=>$error]); 	
           exit();
       }
       else
       {
         $type = explode('.', $_FILES['image']['name']);
         $type = $type[count($type) - 1];
         $image = 'uploads/current_affair/'.$config['file_name'].'.'.$type;
       }
     }else{
      echo json_encode(['status'=>402, 'message'=>'Please select a image']);
      exit();
       //$image = 'public/website/images/dummy_image.jpg';
     }


     $data = array(
      'title'       => $title,
      'type'        => $currentType,
      'description' => $description,
      'date'        => $date,
      'image'       => $image,
      'slug'        => $slug,
     );

     $store = $this->setting_model->store_current_affairs($data);

    if($store){
      echo json_encode(['status'=>200, 'message'=>'Current Affairs added successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function update_current_affairs(){
    $id = $this->input->post('id');
    $title = $this->input->post('title');
    $currentType = $this->input->post('currentType');
    $description = $this->input->post('description');
    $date = $this->input->post('date');
    $currentAffair = $this->setting_model->get_current_affairs(array('id' => $id));

    $slug = str_replace(' ','-',strtolower($title));
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     if(empty($currentType)){
      echo json_encode(['status'=>402, 'message'=>'Please select a Type']);
      exit();
     }

     if(empty($description)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a description']);
      exit();
     }

     if(empty($date)){
      echo json_encode(['status'=>402, 'message'=>'Please select a date']);
      exit();
     }

     $this->load->library('upload');
     if(!empty($_FILES['image']['name'])){

      $config = array(
       'upload_path' 	=> 'uploads/current_affair',
       'file_name' 	=> str_replace(' ','','current_affairs').uniqid(),
       'allowed_types' => 'jpg|jpeg|png|gif',
       'max_size' 		=> '10000000',
      );
      $this->upload->initialize($config);
     if ( ! $this->upload->do_upload('image'))
       {
           $error = $this->upload->display_errors();
           echo json_encode(['status'=>403, 'message'=>$error]); 	
           exit();
       }
       else
       {
         $type = explode('.', $_FILES['image']['name']);
         $type = $type[count($type) - 1];
         $image = 'uploads/current_affair/'.$config['file_name'].'.'.$type;
       }
     }else{
       $image = $currentAffair->image;
     }
       //$image = 'public/website/images/dummy_image.jpg';
     
     
     $data = array(
      'title'       => $title,
      'type'        => $currentType,
      'description' => $description,
      'date'        => $date,
      'image'       => $image,
      'slug'        => $slug
     );

     $update = $this->setting_model->update_current_affairs($data,$id);

    if($update){
      echo json_encode(['status'=>200, 'message'=>'Current Affairs updated successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function delete_current_affairs(){
    $id = $this->input->post('id');

    $delete = $this->setting_model->delete_current_affairs($id);

    if($delete){
      echo json_encode(['status'=>200, 'message'=>'Current Affairs deleted successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }

  }


  public function whats_news() {
    $this->not_admin_logged_in();
    $data['page_title'] = "What's News";
    $this->admin_template('setting/whats-news',$data);
  }

  public function ajaxWhatsNews(){
    $this->not_admin_logged_in();
		$condition = array('status'=>1);
		$whatsNews = $this->setting_model->make_datatables_whatsNews($condition); // this will call modal function for fetching data
		$data = array();
		foreach($whatsNews as $key=>$whatsNew) // Loop over the data fetched and store them in array
		{
    $button = '';
			$sub_array = array();
      $button .= '<a href="'.base_url('edit-whats-news/'.base64_encode($whatsNew['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Update whats news" class="btn btn-sm bg-primary-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_whats_news('.$whatsNew['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete whats news" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			
			$sub_array[] = $key+1;
			$sub_array[] = $whatsNew['title'];
			$sub_array[] = $whatsNew['description'];
			$sub_array[] = date('d-F-Y', strtotime($whatsNew['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->setting_model->get_all_data_whatsNews($condition),
			"recordsFiltered"         =>     $this->setting_model->get_filtered_data_whatsNews($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
  }

  public function create_whats_news(){
    $data['page_title'] = 'Create Whats News';
    $this->admin_template('setting/create-whats-news',$data);
  }

  public function edit_whats_news(){
    $data['page_title'] = 'Edit Whats News';
    $id = base64_decode($this->uri->segment(2));
    $data['news'] = $this->setting_model->get_whats_news(array('id' => $id));
    $this->admin_template('setting/edit-whats-news',$data);
  }

  public function store_whats_news(){
    $title = $this->input->post('title');
    $description = $this->input->post('description');
    $link = $this->input->post('link');

    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     if(empty($description)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a description']);
      exit();
     }

     if(empty($link)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a link']);
      exit();
     }

     $data = array(
      'title'       => $title,
      'description' => $description,
      'link'        => $link
     );

     $store = $this->setting_model->store_whats_news($data);

    if($store){
      echo json_encode(['status'=>200, 'message'=>'Whats News added successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function update_whats_news(){
    $id = $this->input->post('id');
    $title = $this->input->post('title');
    $description = $this->input->post('description');
    $link = $this->input->post('link');
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     if(empty($description)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a description']);
      exit();
     }
  
     if(empty($link)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a link']);
      exit();
     }

     $data = array(
      'title'       => $title,
      'description' => $description,
      'link' => $link
     );

     $update = $this->setting_model->update_whats_news($data,$id);

    if($update){
      echo json_encode(['status'=>200, 'message'=>'Whats News updated successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }


  public function delete_whats_news(){
    $id = $this->input->post('id');

    $delete = $this->setting_model->delete_whats_news($id);

    if($delete){
      echo json_encode(['status'=>200, 'message'=>'Whats News deleted successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }

  }

  public function videos() {
    $this->not_admin_logged_in();
    $data['page_title'] = "Videos";
    $this->admin_template('setting/videos',$data);
  }

  public function ajaxVideos(){
    $this->not_admin_logged_in();
		$condition = array('status'=>1);
		$videos = $this->setting_model->make_datatables_videos($condition); // this will call modal function for fetching data
		$data = array();
		foreach($videos as $key=>$video) // Loop over the data fetched and store them in array
		{
    $button = '';
			$sub_array = array();
      $button .= '<a href="'.base_url('edit-videos/'.base64_encode($video['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Videos" class="btn btn-sm bg-primary-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_videos('.$video['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Videos" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			$video_ifram = '<iframe width="200" height="200" src="http://www.youtube.com/embed/'. $video['video'].'?autoplay=1" frameborder="0"></iframe>';
			$sub_array[] = $key+1;
			$sub_array[] = $video['title'];
			$sub_array[] = $video_ifram;
			$sub_array[] = date('d-F-Y', strtotime($video['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->setting_model->get_all_data_videos($condition),
			"recordsFiltered"         =>     $this->setting_model->get_filtered_data_videos($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
  }

  public function create_videos() {
    $this->not_admin_logged_in();
    $data['page_title'] = "Create Videos";
    $this->admin_template('setting/create-videos',$data);
  }

  public function store_videos(){
    $title = $this->input->post('title');
    $video = $this->input->post('video');
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     if(empty($video)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a video link']);
      exit();
     }

     $data = array(
      'title'  => $title,
      'video'  => $video
     );

     $store = $this->setting_model->store_videos($data);

    if($store){
      echo json_encode(['status'=>200, 'message'=>'Videos added successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function edit_videos() {
    $this->not_admin_logged_in();
    $id = base64_decode($this->uri->segment(2));
    $data['page_title'] = "Edit Videos";
    $data['video'] = $this->setting_model->get_video(array('id' => $id,'status' => 1));
    $this->admin_template('setting/edit-videos',$data);
  }

  public function update_videos(){
    $videoID = $this->input->post('videoID');
    $title = $this->input->post('title');
    $video = $this->input->post('video');
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     if(empty($video)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a video link']);
      exit();
     }

     $data = array(
      'title'  => $title,
      'video'  => $video
     );

     $update = $this->setting_model->update_videos($data, $videoID);

    if($update){
      echo json_encode(['status'=>200, 'message'=>'Videos updates successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function delete_videos(){
    $id = $this->input->post('id');

    $delete = $this->setting_model->delete_videos($id);

    if($delete){
      echo json_encode(['status'=>200, 'message'=>'Video deleted successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }

  }

  public function ajaxDiscount(){
    $this->not_admin_logged_in();
		$condition = array('course_discount.status'=>1);
		$discounts = $this->setting_model->make_datatables_discount($condition); // this will call modal function for fetching data
		$data = array();
		foreach($discounts as $key=>$discount) // Loop over the data fetched and store them in array
		{
    $button = '';
			$sub_array = array();
      $button .= '<a href="'.base_url('edit-discount/'.base64_encode($discount['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Videos" class="btn btn-sm bg-primary-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_discount('.$discount['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Videos" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			$sub_array[] = $key+1;
			$sub_array[] = $discount['title'];
			$sub_array[] = $discount['coupon'];
      $sub_array[] = $discount['courseName'];
      $sub_array[] = $discount['discount_type'];
      $sub_array[] = $discount['discount_type']=='Percentage' ? '% '.$discount['discount'] : '₹ '.$discount['discount'];
      $sub_array[] = $discount['min_order'];
      $sub_array[] = !empty($discount['expiry_date']) ? date('d-m-Y', strtotime($discount['expiry_date'])) : '';
			$sub_array[] = date('d-F-Y', strtotime($discount['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->setting_model->get_all_data_discount($condition),
			"recordsFiltered"         =>     $this->setting_model->get_filtered_data_discount($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
  }

  public function discount(){
    $this->not_admin_logged_in();
    $data['page_title'] = "Discount";
    $data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
    $this->admin_template('setting/discount',$data);
  }

  public function create_discount(){
    $this->not_admin_logged_in();
    $data['page_title'] = "Create Discount";
    $data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
    $this->admin_template('setting/create-discount',$data);
  }



  public function store_discount(){
    $courseID = $this->input->post('courseID');
    $title = $this->input->post('title');
    $discount_type = $this->input->post('discount_type');
    $discount = $this->input->post('discount');
    $min_order = $this->input->post('min_order');
    $coupon = $this->input->post('coupon');
    $expiry_date = $this->input->post('expiry_date');

    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

    if(empty($courseID)){
      echo json_encode(['status'=>402, 'message'=>'Please select a course']);
      exit();
     }

 

     if(empty($discount_type)){
      echo json_encode(['status'=>402, 'message'=>'Please select a discount type']);
      exit();
     }

     if(empty($discount)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a discount']);
      exit();
     }

     if(empty($coupon)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a coupon']);
      exit();
     }
     $check_coupon = $this->setting_model->get_discount(array('course_discount.coupon'=>$coupon));
     if($check_coupon){
      echo json_encode(['status'=>402, 'message'=>'This discount coupon is already in use']);
      exit();
     }

     if(empty($expiry_date)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a expiration date']);
      exit();
     }
   
     $data = array(
      'courseID' => $courseID,
      'title' => $title,
      'discount_type' => $discount_type,
      'discount' => $discount,
      'min_order' => $min_order,
      'coupon' => $coupon,
      'expiry_date' => $expiry_date,
     );

     $store = $this->setting_model->store_discount($data);

    if($store){
      echo json_encode(['status'=>200, 'message'=>'Discount Coupon added successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }

  }

  public function edit_discount(){
    $this->not_admin_logged_in();
    $data['page_title'] = "Edit Discount";
    $id = base64_decode($this->uri->segment(2));
    $data['discount'] = $this->setting_model->get_discount(array('course_discount.id'=>$id));
    $data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
    $this->admin_template('setting/edit-discount',$data);
  }

  public function update_discount(){
    $discountID = $this->input->post('discountID');
    $courseID = $this->input->post('courseID');
    $title = $this->input->post('title');
    $discount_type = $this->input->post('discount_type');
    $discount = $this->input->post('discount');
    $min_order = $this->input->post('min_order');
    $coupon = $this->input->post('coupon');
    $expiry_date = $this->input->post('expiry_date');

    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

    if(empty($courseID)){
      echo json_encode(['status'=>402, 'message'=>'Please select a course']);
      exit();
     }

 

     if(empty($discount_type)){
      echo json_encode(['status'=>402, 'message'=>'Please select a discount type']);
      exit();
     }

     if(empty($discount)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a discount']);
      exit();
     }

     if(empty($coupon)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a coupon']);
      exit();
     }
     $check_coupon = $this->setting_model->get_discount(array('course_discount.coupon'=>$coupon,'course_discount.id <>'=>$discountID));
     if($check_coupon){
      echo json_encode(['status'=>402, 'message'=>'This discount coupon is already in use']);
      exit();
     }

     if(empty($expiry_date)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a expiration date']);
      exit();
     }
   
     $data = array(
      'courseID' => $courseID,
      'title' => $title,
      'discount_type' => $discount_type,
      'discount' => $discount,
      'min_order' => $min_order,
      'coupon' => $coupon,
      'expiry_date' => $expiry_date,
     );

     $update = $this->setting_model->update_discount($data,$discountID);

    if($update){
      echo json_encode(['status'=>200, 'message'=>'Discount Coupon updated successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }

  }

  public function delete_discount(){
    $id = $this->input->post('id');

    $delete = $this->setting_model->delete_discount($id);

    if($delete){
      echo json_encode(['status'=>200, 'message'=>'Discount deleted successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }



}




?>