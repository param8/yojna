<?php 
class MY_Controller extends CI_Controller 
{
	var $permission = array();

	public function __construct() 
	{
	   parent::__construct();
 
    $this->load->model('Common_model');
		$this->load->helper('mail');
		$this->load->helper('sms_helper');
		if(empty($this->session->userdata('logged_in'))) {
			$session_data = array('logged_in' => FALSE);
			$this->session->set_userdata($session_data);
		}
	}

	public function logged_in()
	{
		$session_data = $this->session->userdata();
		
		if($session_data['logged_in'] == TRUE) {
			if($session_data['user_type'] == 'Admin' || $session_data['user_type']=='Staff' || $session_data['user_type']=='Faculties')
			{
				redirect('dashboard', 'refresh');	
			}else{
				redirect('home', 'refresh');
			}	
		}
	}

	public function not_logged_in()
	{
		$session_data = $this->session->userdata();
		if($session_data['logged_in'] == FALSE) {
			redirect('home', 'refresh');
		}
	}

	public function user_verification()
	{
		$session_data = $this->session->userdata();
		if($session_data['verification_user'] == 0) {
			redirect('verification', 'refresh');
		}
	}

	public function not_admin_logged_in()
	{
	
		$session_data = $this->session->userdata();
		//print_r($session_data);die;
		//print_r($session_data);die;
		if($session_data['logged_in'] == FALSE && ($session_data['user_type'] != 'Admin' || $session_data['user_type'] != 'Faculties')) {
			redirect('Authantication', 'refresh');
		}
	}


	public function siteinfo(){
		$siteinfo = $this->Common_model->get_site_info();
	  return $siteinfo; 
	 }



	 public function login_template($page = null, $data = array())
	 {
		$data['siteinfo'] = $this->siteinfo();
		$this->load->view('components/head',$data);
		$this->load->view(''.$page);
		$this->load->view('components/footer');
	 }



	 public function sendEmailMessage($email,$subject,$html){
	   return	sendEmail($email,$subject,$html);
	 }
	
	 public function sendPhoneMessage($phone,$message,$templateID){
	  return	sendSMS($phone,$message,$templateID);
	 }

	 public function admin_template($page = null, $data = array())
	 {
		$data['siteinfo'] = $this->siteinfo();
		$this->load->view('components/head',$data);
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view($page);
		$this->load->view('components/footer');
	 }

	 public function template($page = null, $data = array())
	 {
		$data['siteinfo'] = $this->siteinfo();
		$data['seo'] = $this->seo();
		$this->load->view('components/head',$data);
		$this->load->view('components/header');
		if($data['panel']=='Panel'){
			$this->load->view('components/sidebar');
		}
		$this->load->view($page);
		$this->load->view('components/footer');
	 }

	//  public function user_template($page = null, $data = array())
	//  {
	// 	$data['siteinfo'] = $this->siteinfo();
	// 	$this->load->view('components/head',$data);
	// 	$this->load->view('components/header');
	// 	$this->load->view('components/sidebar');
	// 	$this->load->view($page);
	// 	$this->load->view('components/user_footer');
	//  }


}