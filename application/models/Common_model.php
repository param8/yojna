<?php 
class Common_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

  public function get_site_info(){
    //$adminID = $this->session->userdata('user_type')=='Teacher' ? $this->session->userdata('id') : 1;
    $this->db->where('adminID', 1);
    return $this->db->get('site_info')->row();
  }

  public function get_seo(){
    $this->db->where('id', 1);
    return $this->db->get('seo_setting')->row();
  }
 
  public function get_email_info(){
    //$this->db->where('status', 1);
    return $this->db->get('email_setting')->row();
  }

  public function get_payment_info(){
    return $this->db->get('payment_setting')->row();
  }

  public function get_seo_info(){
    return $this->db->get('seo_setting')->row();
  }

  public function get_about(){
   return $this->db->get('about_us')->row();
  }

  public function get_country()
  {
    return $this->db->get('countries')->result();
  }

  public function get_country_by_id($condition)
  {
    $this->db->where($condition);
    return $this->db->get('countries')->row();
  }

  public function get_states($condition)
  {
      $this->db->where($condition);
      $this->db->order_by('name', 'ASC');
      return $this->db->get('states')->result();
  }

  public function get_city($condition)
  {
      $this->db->where($condition);
      $this->db->order_by('name', 'ASC');
      return $this->db->get('cities')->result();
  }

  public function get_state_id($condition)
  {
      $this->db->where($condition);
      return $this->db->get('states')->row();
  }

  public function get_city_id($condition)
  {
      $this->db->where($condition);
      return $this->db->get('cities')->row();
  }
  public function get_country_id($condition){
    $this->db->where($condition);
      return $this->db->get('countries')->row();
  }

  public function store_city($city_data){
  $this->db->insert('cities', $city_data);
  return $this->db->insert_id();
}

public function store_state($state_data){
  $this->db->insert('states', $state_data);
  return $this->db->insert_id();
}

public function get_permission($condition){
  $this->db->where($condition);
  return $this->db->get('permission')->row();
}

public function store_order($data){
  $this->db->insert('orders', $data);
  return $this->db->insert_id();
}

public function store_item($data){
  return $this->db->insert('items', $data);
}

public function get_order($condition){
  $this->db->where($condition);
  return $this->db->get('orders')->row();
}

public function get_orders($condition){
  $this->db->select('items.*,orders.orderID as order_id,users.name as userName,orders.payment_status as paymentStatus,orders.status as orderStatus,courses.name as courseName,courses.image as coursePic');
  $this->db->from('orders');
  $this->db->join('items', 'orders.id=items.orderID','left');
  $this->db->join('courses', 'courses.id=items.courseID','left');
  $this->db->join('users', 'users.id=orders.userID','left');
  $this->db->where($condition);
  $this->db->order_by('orders.id', 'desc');
  return $this->db->get()->result();
}


}
