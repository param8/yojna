<?php 

class Scheme_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

  
	function make_query_condition($condition)
  {
    $this->db->select('eligibility_conditions.*');
    $this->db->from('eligibility_conditions');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('id','desc');
    
  }
    function make_datatables_condition($condition){
	  $this->make_query_condition($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data_condition($condition){
	  $this->make_query_condition($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_condition($condition)
  {
    $this->db->select('eligibility_conditions.*');
    $this->db->from('eligibility_conditions');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
  }

  public function get_eligibility_conditions($condition){
    $this->db->where($condition);
    return $this->db->get('eligibility_conditions')->row();
  }

  public function store_eligibility_conditions($data){
    return $this->db->insert('eligibility_conditions',$data);
  }

  public function update_eligibility_conditions($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('eligibility_conditions',$data);
  }



  // Scheme Documents

  function make_query_documents($condition)
  {
    $this->db->select('scheme_documents.*');
    $this->db->from('scheme_documents');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('id','desc');
    
  }
    function make_datatables_documents($condition){
	  $this->make_query_documents($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data_documents($condition){
	  $this->make_query_documents($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_documents($condition)
  {
    $this->db->select('scheme_documents.*');
    $this->db->from('scheme_documents');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
  }

  public function get_scheme_documents($condition){
    $this->db->where($condition);
    return $this->db->get('scheme_documents')->row();
  }

  public function store_scheme_documents($data){
    return $this->db->insert('scheme_documents',$data);
  }

  public function update_scheme_documents($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('scheme_documents',$data);
  }

  
}