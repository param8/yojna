<?php 

class User_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

  
	function make_query($condition)
  {
    $this->db->select('users.*,cities.name as cityName,states.name as stateName,user_detail.subject as user_subject,user_detail.dob,user_detail.gender,user_detail.about');
    $this->db->from('users');
    $this->db->join('cities','cities.id = users.city','left');
    $this->db->join('states','states.id = users.state','left'); 
    $this->db->join('user_detail','user_detail.userID = users.id','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('users.email', $_POST["search"]["value"]);
    $this->db->or_like('users.phone', $_POST["search"]["value"]);
    $this->db->or_like('cities.name', $_POST["search"]["value"]);
    $this->db->or_like('states.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('users.id','desc');
    
  }
    function make_datatables($condition){
	  $this->make_query($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
    $this->db->select('users.*,cities.name as cityName,states.name as stateName,user_detail.subject as user_subject,user_detail.dob,user_detail.gender,user_detail.about');
    $this->db->from('users');
    $this->db->join('cities','cities.id = users.city','left');
    $this->db->join('states','states.id = users.state','left'); 
    $this->db->join('user_detail','user_detail.userID = users.id','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('users.email', $_POST["search"]["value"]);
    $this->db->or_like('users.phone', $_POST["search"]["value"]);
    $this->db->or_like('cities.name', $_POST["search"]["value"]);
    $this->db->or_like('states.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('users.id','desc');
	   return $this->db->count_all_results();
  }

  public function get_user($condition){
    $this->db->select('users.*,cities.name as cityName,states.name as stateName,user_detail.subject as user_subject,user_detail.dob,user_detail.gender,user_detail.about');
    $this->db->from('users');
    $this->db->join('cities','cities.id = users.city','left');
    $this->db->join('states','states.id = users.state','left'); 
    $this->db->join('user_detail','user_detail.userID = users.id','left');
	$this->db->where($condition);
	return $this->db->get()->row();
  }
  public function get_users($condition){
    $this->db->select('users.*,cities.name as cityName,states.name as stateName,user_detail.subject as user_subject,user_detail.dob,user_detail.gender,user_detail.about');
    $this->db->from('users');
    $this->db->join('cities','cities.id = users.city','left');
    $this->db->join('states','states.id = users.state','left'); 
    $this->db->join('user_detail','user_detail.userID = users.id','left');
	$this->db->where($condition);
	return $this->db->get()->result();
  }

  public function store_user($data){
	 $this->db->insert('users',$data);
   return $this->db->insert_id();
  }

  public function store_verification($data){
    return $this->db->insert('verification',$data);
   }

  public function update_user($data,$userID){
	$this->db->where('id',$userID);
	return $this->db->update('users',$data);
  }

  public function store_user_detail($data){
    return $this->db->insert('user_detail',$data);
  }

  public function update_user_detail($data,$userID){
    $this->db->where('userID',$userID);
	return $this->db->update('user_detail',$data);
	 //echo $this->db->last_query();
	  
  }

  public function get_user_detail($detail){

    $this->db->where('email',$detail);
    $this->db->or_where('phone',$detail);
   return $this->db->get('users');

  }

  public function get_faculties($condition,$value){
    $this->db->select('users.*,cities.name as cityName,states.name as stateName,user_detail.subject as user_subject,user_detail.dob,user_detail.gender,user_detail.about');
    $this->db->from('users');
    $this->db->join('cities','cities.id = users.city','left');
    $this->db->join('states','states.id = users.state','left'); 
    $this->db->join('user_detail','user_detail.userID = users.id','left');
	  $this->db->where($condition);
    if(!empty($value)){
      $this->db->like('users.name',$value);
      $this->db->or_like('users.address',$value);
      $this->db->or_like('users.email',$value);
      $this->db->or_like('user_detail.subject',$value);
    }
	  return $this->db->get()->result();
  }

}