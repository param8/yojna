<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
             
              <div class="card-body pt-0">
                <form id="editUpcomingExamForm" method="post" action="<?=base_url('Common/update_upcomingExam');?>" enctype="multipart/form-data">
                  <div class="settings-form row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Title <span class="star-red">*</span></label>
                        <input type="text" name="title" id="title" class="form-control" value="<?=$upcomingExams->title?>" placeholder="Enter Title">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Link <span class="star-red">*</span></label>
                        <input type="text" name="link" id="link" class="form-control" value="<?=$upcomingExams->link?>" placeholder="Enter Link">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <p class="settings-label">Image <span class="star-red">*</span></p>
                        <div class="settings-btn">
                          <input type="file" accept="image/*" name="image" id="file" onchange="loadFile(event)" class="hide-input">
                          <label for="file" class="upload">
                          <i class="feather-upload"></i>
                          </label>
                        </div>
                        <img src="<?=base_url($upcomingExams->image)?>" class="settings-size" style="height:100px;width:100px;">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Description <span class="star-red">*</span></label>
                        <textarea class="form-control"name="description" id="description"><?=$upcomingExams->description?></textarea>
                      </div>
                    </div>
                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Update</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $("form#editUpcomingExamForm").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault();    
  			var formData = new FormData(this);
            formData.append('id',<?=$upcomingExams->id?>)
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
  
          location.href="<?=base_url('upcoming-exam')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});
  
      $('#description').summernote({
            height:300,
        });
  
</script>