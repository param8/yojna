<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <div class="col-sm-6">
            <div class="float-right">
             <a type="button" class="btn btn-primary btn-sm" href="<?=base_url('create-student-remark')?>" style="float: right">Create <?=$page_title?></a>
           </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class=" table table-hover table-center mb-0" id="studentRemarkDataTable">
                  <thead>
                    <tr>
                      <th>S.no.</th>
                      <th>Thumnail Image</th>
                      <th>Title</th>
                      <th>Created at</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
     //alert('dfgfgf');
       // $('#example').DataTable();
       // } );
       var dataTable = $('#studentRemarkDataTable').DataTable({
           "processing": true,
           "serverSide": true,
           buttons: [{
               extend: 'excelHtml5',
               text: 'Download Excel'
           }],
           "order": [],
           "ajax": {
               url: "<?=base_url('Common/ajaxStudentRemark')?>",
               type: "POST"
           },
           "columnDefs": [{
               "targets": [0],
               "orderable": false,
           }, ],
       });
   });

   function delete_student_remark(id){
   Swal.fire({
           title: 'Are you sure?',
           text: "You won't be able to revert this!",
           type: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Yes, delete it!'
       }).then((result) => {
           if (result.value) {
               $.ajax({
                   url: '<?=base_url('Common/delete_student_remark')?>',
                   type: 'POST',
                   data: {
                     id
                   },
                   dataType: 'json',
                   success: function(data) {
                       if (data.status == 200) {
                         toastNotif({
                         text: data.message,
                         color: '#5bc83f',
                         timeout: 5000,
                         icon: 'valid'
                       });
                       setTimeout(function(){
  
                         location.href = "<?=base_url('student-remark')?>"	
  
                         }, 1000) 
                         
                       } else if (data.status == 302) {
                         toastNotif({
                         text: data.message,
                         color: '#da4848',
                         timeout: 5000,
                         icon: 'error'
                       });
  
                       }
                   }
               })
           }
       })
  }
  
  </script>