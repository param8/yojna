    <script src="<?=base_url('public/admin/assets/js/jquery-3.6.0.min.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/js/bootstrap.bundle.min.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/js/feather.min.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/plugins/slimscroll/jquery.slimscroll.min.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/plugins/raphael/raphael.min.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/plugins/morris/morris.min.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/plugins/select2/js/select2.min.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/js/chart.morris.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/js/script.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/plugins/datatables/jquery.dataTables.min.js')?>"></script>
    <script src="<?=base_url('public/admin/assets/plugins/datatables/datatables.min.js')?>"></script>
    <script>
       $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
    </script>
  </body>
</html>