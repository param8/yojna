
<body>
    <div class="main-wrapper">
      <div class="header">
        <div class="header-left">
          <a href="index.html" class="logo">
          <img src="<?=base_url($siteinfo->site_logo)?>" alt="Logo">
          </a>
          <a href="<?=base_url('dashboard')?>" class="logo logo-small">
          <img src="<?=base_url($siteinfo->site_logo)?>" alt="Logo" width="30" height="30">
          </a>
        </div>
        <a href="javascript:void(0);" id="toggle_btn">
        <i class="fe fe-text-align-left"></i>
        </a>
        <!-- <div class="top-nav-search">
          <form>
            <input type="text" class="form-control" placeholder="Search here">
            <button class="btn" type="submit"><i class="fa fa-search"></i></button>
          </form>
        </div> -->
        <a class="mobile_btn" id="mobile_btn">
        <i class="fa fa-bars"></i>
        </a>
        <ul class="nav user-menu">
          <!-- <li class="nav-item dropdown noti-dropdown">
            <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
            <i class="fe fe-bell"></i> <span class="badge badge-pill">3</span>
            </a>
            <div class="dropdown-menu notifications">
              <div class="topnav-dropdown-header">
                <span class="notification-title">Notifications</span>
                <a href="javascript:void(0)" class="clear-noti"> Clear All </a>
              </div>
              <div class="noti-content">
                <ul class="notification-list">
                  <li class="notification-message">
                    <a href="#">
                      <div class="media d-flex">
                        <span class="avatar avatar-sm flex-shrink-0">
                        <img class="avatar-img rounded-circle" alt="User Image" src="assets/img/user/user.jpg">
                        </span>
                        <div class="media-body flex-grow-1">
                          <p class="noti-details"><span class="noti-title">Jonathan Doe</span> Schedule <span class="noti-title">his appointment</span></p>
                          <p class="noti-time"><span class="notification-time">4 mins ago</span></p>
                        </div>
                      </div>
                    </a>
                  </li>
                </ul>
              </div>
              <div class="topnav-dropdown-footer">
                <a href="#">View all Notifications</a>
              </div>
            </div>
          </li> -->
          <li class="nav-item dropdown has-arrow">
            <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
            <span class="user-img"><img class="rounded-circle" src="<?=base_url($this->session->userdata('profile_pic'))?>" width="31" alt="<?=$this->session->userdata('name')?>"></span>
            </a>
            <div class="dropdown-menu">
              <div class="user-header">
                <div class="avatar avatar-sm">
                  <img src="<?=base_url($this->session->userdata('profile_pic'))?>" alt="User Image" class="avatar-img rounded-circle">
                </div>
                <div class="user-text">
                  <h6><?=$this->session->userdata('name')?></h6>
                  <p class="text-muted mb-0"><?=$this->session->userdata('user_type')?></p>
                </div>
              </div>
              <a class="dropdown-item" href="">My Profile</a>
              <a class="dropdown-item" href="">Settings</a>
              <a class="dropdown-item" href="<?=base_url('Authantication/logout')?>">Logout</a>
            </div>
          </li>
        </ul>
      </div>