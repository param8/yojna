<div class="sidebar" id="sidebar">
        <div class="sidebar-inner slimscroll">
          <div id="sidebar-menu" class="sidebar-menu">
            <ul>
              <!-- <li class="menu-title">
                <span><i class="fe fe-home"></i> Main</span>
              </li> -->
              <li class="active">
                <a href="<?=base_url('dashboard')?>"><span><i class="fe fe-home"></i> Dashboard</span></a>
              </li>

              <li>
              <a href="<?=base_url('eligibility-conditions')?>"><span><i class="fa fa-info-circle" style="font-size:18px;"></i> Eligibility Conditions</span></a>
              </li>
              <li>
                <a href="<?=base_url('scheme-documents')?>"><span><i class="fas fa-file" style="font-size:18px;"></i> Scheme  Documents </span></a>
              </li>
              <li>
                <a href="<?=base_url('scheme')?>"><span><i class="fa fa-tasks" style="font-size:18px;"></i> Scheme</span></a>
              </li>
              
              <li>
               <a href="<?=base_url('setting')?>"><span><i class="fa fa-cog" style="font-size:16px" aria-hidden="true"></i> Setting</span></a>
            </li>
             
            </ul>
          </div>
        </div>
      </div>