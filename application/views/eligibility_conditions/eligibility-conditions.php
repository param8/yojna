<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">

      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <div class="col-sm-6">
          <div class="float-right">
           <a href="javascript:void(0)" class="btn btn-primary btn-sm"  onclick="addModalForm()" style="float:right">Create <?=$page_title?></a>
          </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class=" table table-hover table-center mb-0" id="conditionDataTable">
                  <thead>
                    <tr>
                      <th>S.no.</th>
                      <th>Action</th>
                      <th>Name</th>
                      <th>Created_at</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Add Modal Eligibility Conditions -->
<div class="modal fade" id="addModalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus text-primary"></i> Create <?=$page_title?> </h5>
        <button type="button" class="close"  onclick="close_modal()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="createConditionForm" action="<?=base_url('Scheme/create_eligibility_conditions')?>" method="POST">
        <div class="modal-body">
          <div class="row">
            <div class="col-xl-12">
              <div class="form-group row">
                <label class="col-lg-12 col-md-12 col-xl-12 col-form-label"><?=$page_title?></label>
                <div class="col-lg-12 col-md-12 col-xl-12">
                  <input type="text" class="form-control" id="name" name="name">
                </div>
              </div>
            </div>
        
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Edit Modal Eligibility Conditions -->

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit text-warning"></i> Edit <?=$page_title?> </h5>
        <button type="button" class="close" onclick="close_modal()">
          <span aria-hidden="true">&times;</spa n>
        </button>
      </div>
      <form id="editConditionForm" action="<?=base_url('Scheme/update_eligibility_conditions')?>" method="POST">
        <div class="modal-body" id="editFormData">
      
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Edit</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  //alert('dfgfgf');
  // $('#example').DataTable();
  // } );
  var dataTable = $('#conditionDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Scheme/ajaxEligibilityConditions')?>",
      type: "POST"
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }, ],
  });
});
function close_modal(){
  $('#addModalForm').modal('hide');
  $('#editModal').modal('hide');
}
function addModalForm(){
  $('#addModalForm').modal('show');
}

$("form#createConditionForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.reload();

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


function editModal(id){
  $.ajax({
    url: "<?=base_url('Scheme/eligibility_conditions_edit_form')?>",
    type: 'POST',
    data: {id},
   
    dataType: 'html',
    success: function(data) {
      $('#editModal').modal('show');
      $('#editFormData').html(data);
    },
   
  });
}


$("form#editConditionForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.reload();

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


</script>