
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title">Website Basic Details</h5>
          </div>
          <div class="card-body pt-0">
            <form id="product_form" method="post" action="<?=base_url('Store-general-form');?>" enctype="multipart/form-data">
              <div class="settings-form row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Website Name <span class="star-red">*</span></label>
                    <input type="text" name="site_name" class="form-control" value="<?=$siteinfo->site_name?>" placeholder="Enter Website Name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Email <span class="star-red">*</span></label>
                    <input type="email" name="email" class="form-control" value="<?=$siteinfo->site_email?>" placeholder="Enter Email">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Contact Number <span class="star-red">*</span></label>
                    <input type="number" class="form-control" name="mobile" value="<?=$siteinfo->site_contact?>" placeholder="Enter Contact Number">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Whatsapp Number <span class="star-red">*</span></label>
                    <input type="number" class="form-control" name="whatsapp" value="<?=$siteinfo->whatsapp_no?>" placeholder="Enter Whatsapp Number">
                  </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Footer Content <span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="footer_content" value="<?=$siteinfo->footer_contant?>" placeholder="Enter Footer Content">
                    </div>
                  </div>  
                <div class="col-md-6">
                  <div class="form-group">
                    <p class="settings-label">Logo <span class="star-red">*</span></p>
                    <div class="settings-btn">
                      <input type="file" accept="image/*" name="image" id="file" onchange="loadFile(event)" value="<?=$siteinfo->site_logo?>" class="hide-input">
                      <label for="file" class="upload">
                      <i class="feather-upload"></i>
                      </label>
                    </div>
                    <h6 class="settings-size">Recommended image size is <span>150px x 150px</span></h6>
                    <div class="upload-images">
                      <img src="<?=$siteinfo->site_logo?>" alt="Image">
                      <a href="javascript:void(0);" class="btn-icon logo-hide-btn">
                      <i class="feather-x-circle"></i>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                        <label>Description <span class="star-red">*</span></label>
                        <textarea class="form-control"name="description"><?=$siteinfo->discription?></textarea>
                    </div>
                  </div>
                             
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Address </label>
                    <textarea class="form-control" name="address"><?=$siteinfo->site_address?></textarea>
                  </div>
                </div>                
                
                <div class="form-group mb-0">
                  <div class="settings-btns">
                    <button type="submit" class="btn btn-orange">Update</button>
                    <button type="reset" class="btn btn-grey">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>

<script>
$("form#product_form").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) { 
				if(data.status==200) {
				//$('.modal').modal('hide');
        toastNotif({
				text: data.message,
				color: '#5bc83f',
				timeout: 5000,
				icon: 'valid'
			});
      $(':input[type="submit"]').prop('disabled', false);
      setTimeout(function(){

        location.href="<?=base_url('setting')?>"; 	
        
      }, 1000) 		
				}else if(data.status==403) {
          toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });

          $(':input[type="submit"]').prop('disabled', false);
				}else{
          toastNotif({
              text: data.message,
              color: '#da4848',
              timeout: 5000,
              icon: 'error'
            });
          $(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});


</script>