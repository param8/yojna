<body>
  <div class="main-wrapper login-body">
    <div class="login-wrapper">
      <div class="container">
        <div class="loginbox">
          <div class="login-left">
            <img class="img-fluid" src="<?=base_url($siteinfo->site_logo)?>" alt="Logo">
          </div>
          <div class="login-right">
            <div class="login-right-wrap">
              <h1>Login</h1>
              <p class="account-subtitle">Access to our dashboard</p>
              <form action="<?=base_url('Authantication/login')?>" id="adminLoginForm" method="post">
                <div class="form-group">
                  <input class="form-control" type="text" name="email" id="email" placeholder="Email">
                </div>
                <div class="form-group">
                  <input  class="form-control" type="password" name="password" id="password" placeholder="Password">
                </div>
                <div class="form-group">
                  <button class="btn btn-primary btn-block w-100" type="submit">Login</button>
                </div>
              </form>
              <div class="text-center forgotpass"><a href="forgot-password.html">Forgot Password?</a></div>
            
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
  $("form#adminLoginForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
    toastNotif({
				text: data.message,
				color: '#5bc83f',
				timeout: 5000,
				icon: 'valid'
			});
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){
    //  console.log(data.user_type);
     if(data.user_type=='Student'){
       if(data.verifivation_status == 1){
        location.href="<?=base_url('student-dashboard')?>";
       }else{
        location.href="<?=base_url('verification')?>";
       }
    }else if(data.user_type=='Faculties'){
      location.href="<?=base_url('faculty-dashboard')?>";
    }else{
      location.href="<?=base_url('dashboard')?>"; 	
    }
    
  }, 1000) 
  
  }else if(data.status==403) {
    toastNotif({
				text: data.message,
				color: '#da4848',
				timeout: 5000,
				icon: 'error'
			});

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastNotif({
				text: data.message,
				color: '#da4848',
				timeout: 5000,
				icon: 'error'
			});
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  </script>
