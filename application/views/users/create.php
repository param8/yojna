<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col">
          <h3 class="page-title"><?=$page_title?></h3>
          <ul class="breadcrumb">
            <li class=""><a href="<?=base_url('dashboard')?>">Dashboard</a></li>/
            <li class="breadcrumb-item active"><?=$page_title?></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Enter Basic Detail</h4>
          </div>
          <div class="card-body">
            <form action="<?=base_url('Authantication/store')?>" id="userAddForm" method="post">
              <div class="form-group">
                <label>Full Name <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="name" id="name">
              </div>
              
              <div class="form-group">
                <label>Email Address <span class="text-danger">*</span></label>
                <input type="email" class="form-control" name="email" id="email">
              </div>

              <div class="form-group">
                <label>Contact No. <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="phone" id="phone">
              </div>
              <div class="form-group">
                <label>User Role <span class="text-danger">*</span></label>
                <?php $roles = array('Staff','Student','Faculties');?>
                <select class="form-control" name="user_type" id="user_type">
                  <option value="">Select Role</option>
                  <?php foreach($roles as $role){?>
                    <option value="<?=$role?>"><?=$role?></option>
                  <?php } ?>
                </select>
              </div>
             <input type="hidden" name="created_by" id="created_by" value="Admin">
              <div class="text-end">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
</div>
<script>
  $("form#userAddForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
    toastNotif({
				text: data.message,
				color: '#5bc83f',
				timeout: 5000,
				icon: 'valid'
			});
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('create')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
    toastNotif({
				text: data.message,
				color: '#da4848',
				timeout: 5000,
				icon: 'error'
			});

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastNotif({
				text: data.message,
				color: '#da4848',
				timeout: 5000,
				icon: 'error'
			});
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  </script>
