<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?> Personal Information</h4>
              </div>
              <div class="card-body">
                <form action="<?=base_url('Student/update')?>" id="updateForm" method="POST" enctype="multipart/form-data">
                  <div class="row">
                    <div class="col-xl-6">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Name</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" id="name" name="name" value="<?=$user->name?>">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Email</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" value="<?=$user->email?>" readonly>
                        </div>
                      </div>
                      <!-- <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Gender</label>
                        <div class="col-lg-9">
                        <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="gender_male" value="option1" checked>
                        <label class="form-check-label" for="gender_male">
                        Male
                        </label>
                        </div>
                        <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="gender_female" value="option2">
                        <label class="form-check-label" for="gender_female">
                        Female
                        </label>
                        </div>
                        </div>
                        </div> -->
                      <!-- <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Blood Group</label>
                        <div class="col-lg-9"> -->
                      <!-- <select class="select">
                        <option>Select</option>
                        <option value="1">A+</option>
                        <option value="2">O+</option>
                        <option value="3">B+</option>
                        <option value="4">AB+</option>
                        </select> -->
                      <!-- </div>
                        </div> -->
                    </div>
                    <div class="col-xl-6">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Phone</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" value="<?=$user->phone?>" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Profile Pic</label>
                        <div class="col-lg-9">
                          <input type="file" class="form-control" name="profile_pic" id="profile_pic" accept="image/*">
                          <img src="<?=base_url($user->profile_pic);?>"  height="50" width="50">
                        </div>
                      </div>
                      <!--<div class="form-group row">
                        <label class="col-lg-3 col-form-label">Phone</label>
                        <div class="col-lg-9">
                        <input type="text" class="form-control" value="">
                        </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Password</label>
                        <div class="col-lg-9">
                        <input type="password" class="form-control">
                        </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Repeat Password</label>
                        <div class="col-lg-9">
                        <input type="password" class="form-control">
                        </div>
                        </div> -->
                    </div>
                  </div>
                  <h4 class="card-title">Address</h4>
                  <div class="row">
                
                    <div class="col-xl-4">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">State</label>
                        <div class="col-lg-9">
                          
                          <!-- <input type="text" class="form-control" value="<?//=$staff->stateName?>"> -->
                          <select class="select" name="state" id="state" onchange="getCity(this.value)">
                            <option>Select state</option>
                            <?php 
                              foreach($states as $state){
                              ?>
                            <option value="<?=$state->id?>" <?=$user->state==$state->id ? 'selected' : ''?>><?=$state->name?></option>
                            <?php }?>
                          </select>
                        </div>
                      </div>
                      </div>
                      <div class="col-xl-4">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">City</label>
                        <div class="col-lg-9">
                          <!-- <input type="text" class="form-control" value="<?//=$staff->cityName?>"> -->
                          <select class="select" name="city" id="city">
                          </select>
                        </div>
                      </div>
                      </div>
                      <div class="col-xl-4">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Pincode</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" name="pincode" id="pincode" value="<?=$user->pincode?>">
                          
                        </div>
                      </div>
                      </div>
                      <div class="col-xl-12">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Address</label>
                        
                          <textarea type="text" class="form-control" id="address" name="address" value=""><?=$user->address?></textarea>
                     
                      </div>
                 
                    </div>
                      <!-- <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Postal Code</label>
                        <div class="col-lg-9">
                        <input type="text" class="form-control">
                        </div>
                        </div> -->
                    </div>
                  </div>
                  <div class="text-end">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $("form#updateForm").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault();    
  			var formData = new FormData(this);
        formData.append("userID", '<?=$user->id?>');   
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
  
          location.href="<?=base_url('students')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});
  
  function updateStatus(status){
     alert(status);
  }
  
  function getCity(stateID){
    var userID = <?=$user->id?>;
    $.ajax({
      url: '<?=base_url('Ajax_controller/get_city')?>',
      type: 'POST',
      data: {stateID,userID},
      success: function (data) {
        $('#city').html(data);
      }
      });
  }

  $( document ).ready(function() {
    getCity(<?=$user->state?>)
});
</script>