<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?> Personal Information</h4>
              </div>
              <div class="card-body">
                <form action="#">
                  <div class="row">
                    <div class="col-xl-6">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Name</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" value="<?=$user->name?>" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Email</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" value="<?=$user->email?>" readonly>
                        </div>
                      </div>
                      <!-- <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Gender</label>
                        <div class="col-lg-9">
                        <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="gender_male" value="option1" checked>
                        <label class="form-check-label" for="gender_male">
                        Male
                        </label>
                        </div>
                        <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="gender_female" value="option2">
                        <label class="form-check-label" for="gender_female">
                        Female
                        </label>
                        </div>
                        </div>
                        </div> -->
                      <!-- <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Blood Group</label>
                        <div class="col-lg-9"> -->
                      <!-- <select class="select">
                        <option>Select</option>
                        <option value="1">A+</option>
                        <option value="2">O+</option>
                        <option value="3">B+</option>
                        <option value="4">AB+</option>
                        </select> -->
                      <!-- </div>
                        </div> -->
                    </div>
                    <div class="col-xl-6">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Phone</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" value="<?=$user->phone?>" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Profile Pic</label>
                        <div class="col-lg-9">
                          <img src="<?=base_url($user->profile_pic);?>"  height="50" width="50">
                        </div>
                      </div>
                      <!--<div class="form-group row">
                        <label class="col-lg-3 col-form-label">Phone</label>
                        <div class="col-lg-9">
                        <input type="text" class="form-control" value="">
                        </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Password</label>
                        <div class="col-lg-9">
                        <input type="password" class="form-control">
                        </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Repeat Password</label>
                        <div class="col-lg-9">
                        <input type="password" class="form-control">
                        </div>
                        </div> -->
                    </div>
                  </div>
                  <h4 class="card-title">Address</h4>
                  <div class="row">
                    <div class="col-xl-6">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">State</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" value="<?=$user->stateName?>" readonly>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-6">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">City</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" value="<?=$user->cityName?>" readonly>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-12">
                      <div class="form-group row">
                        <label class="col-lg-1 col-form-label">Address</label>
                        <div class="col-lg-11">
                          <textarea type="text" class="form-control" value="" readonly><?=$user->address?></textarea>
                        </div>
                      </div>
                      <!-- <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Postal Code</label>
                        <div class="col-lg-9">
                        <input type="text" class="form-control">
                        </div>
                        </div> -->
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>