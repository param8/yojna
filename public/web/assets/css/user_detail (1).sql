-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2023 at 08:41 AM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shikshakul`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE `user_detail` (
  `id` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `dob` varchar(50) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_detail`
--

INSERT INTO `user_detail` (`id`, `userID`, `subject`, `gender`, `dob`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, NULL, 1, '2023-06-27 06:31:54', '2023-06-27 06:31:54'),
(2, 2, NULL, NULL, NULL, 1, '2023-06-27 06:31:54', '2023-06-27 06:31:54'),
(3, 3, 'English', NULL, '', 1, '2023-06-27 06:31:54', '2023-06-27 09:28:31'),
(4, 4, 'English', NULL, '', 1, '2023-06-27 06:31:54', '2023-06-27 09:28:07'),
(5, 5, 'Hindi', NULL, '', 1, '2023-06-27 06:31:54', '2023-06-27 09:27:41'),
(6, 6, 'History', NULL, '', 1, '2023-06-27 06:31:54', '2023-06-27 09:24:44'),
(7, 7, 'Mathematics', NULL, '', 1, '2023-06-27 06:31:54', '2023-06-27 09:23:38'),
(8, 8, 'English', NULL, '', 1, '2023-06-27 06:31:54', '2023-06-27 09:22:34'),
(9, 9, 'Geography', NULL, '', 1, '2023-06-27 06:31:54', '2023-06-27 09:21:54'),
(10, 10, 'Hindi', NULL, '', 1, '2023-06-27 06:31:54', '2023-06-27 09:17:16'),
(11, 11, 'English', NULL, '', 1, '2023-06-27 06:31:54', '2023-06-27 09:37:19'),
(12, 12, 'English', 'Male', '2023-06-27', 1, '2023-06-27 06:31:54', '2023-06-27 08:34:45'),
(13, 13, NULL, NULL, NULL, 1, '2023-06-27 06:31:54', '2023-06-27 06:31:54'),
(14, 17, NULL, NULL, NULL, 1, '2023-06-27 06:31:54', '2023-06-27 06:31:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_detail`
--
ALTER TABLE `user_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
